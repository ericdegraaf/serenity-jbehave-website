angular.module('demo', [])
    .controller('CustomerList', function($scope, $http) {
        $scope.activeButton = 'view';
        $scope.disabled = true;
        $http.get('http://localhost:8081/customers/').
            then(function(response) {
                $scope.customerList = response.data;
            });
    });
